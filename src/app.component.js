import React from "react";
import { AppRouter } from "./app.routing";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export const App = () => {
  return (
    <>
      <AppRouter />
      <ToastContainer />
    </>
  );
};

//content supply garne file

//component ===> basic building blocks of reacts
//component can be functional as welll as class
//functional -> stateless
//class -> statefull
//props -> incoming data for an component
//state -> data within a component
