const { toast } = require("react-toastify");

function showSuccess(msg) {
  toast.success(msg);
}

function showInfo(msg) {
  toast.info(msg);
}

function showWarn(msg) {
  toast.warning(msg);
}

function handleError(error) {
  debugger;
  // const err = error.response;
  let errMsg = "Something went wrong!";
  if (error.response.data) {
    errMsg = error.response.data.msg;
  }
  //error
  //check error
  //parse error
  // extract error msg
  //prepare error msg
  //showerror
  toast.error(errMsg);
}

export default {
  showInfo,
  showSuccess,
  showWarn,
  handleError
}
