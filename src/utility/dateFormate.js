import moment from "moment"

function formatDate(date) {
    return moment(date).format("ddd MM-DD-YYYY")
}
function formatTime(date) {
    return moment(date).format("hh:mm a")
}

export default {
    formatDate,
    formatTime
}