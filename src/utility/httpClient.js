import axios from "axios"
const BaseURL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BaseURL,
    responseType: "json"
})

function GetHeaaders(secured) {
    let option = {
        "Content-Type": "application/json",
    }
    if (secured) {
        option['Authorization'] = sessionStorage.getItem("token")
    }
    return option;
}

function GET(url, isSecured = false, params = {}) {
    return http.get(url, {
        headers: GetHeaaders(isSecured),
        params
    })
}

function POST(url, data, isSecured = false, params = {}) {
    return http.post(url, data, {
        headers: GetHeaaders(isSecured),
        params,
    })
}

function PUT(url, data, isSecured = false, params = {}) {
    return http.put(url, data, {
        headers: GetHeaaders(isSecured),
        params
    })
}

function DELETE(url, isSecured = false, params = {}) {
    return http.delete(url, {
        headers: GetHeaaders(isSecured),
        params
    })
}

function UPLOADS(method, url, data = {}, files = []) {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest()
        const formData = new FormData()

        if (files.length) {
            files.forEach((item, i) => {
                formData.append("image", files[i], files[i].name)
            })
        }

        for (let key in data) {
            formData.append(key, data[key])
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                // console.log("Cycle completed")
                // console.log("XHR REQUEST IS", xhr.response)
                if (xhr.status === 200) {
                    resolve(xhr.response)
                }
                else {
                    reject(xhr.response)
                }
            }
        }

        xhr.open(method, `${BaseURL}${url}?token=${localStorage.getItem("token")}`, true)
        xhr.send(formData)
    })

}

export default {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOADS
}