import React, { Component } from 'react'
import { Button } from '../../common/button/button.component';
const ImageURL = process.env.REACT_APP_IMAGE_URL
const defaultData = {
    name: "",
    price: "",
    category: "",
    description: "",
    brand: "",
    color: "",
    size: "",
    modelno: "",
    discountedItem: false,
    discountType: "",
    discountValue: "",
    warrentyStatus: false,
    warrentyPeriod: "",
    image: [],
    offers: [],
    tags: [],
    reviews: []
}
export class ProductForm extends Component {
    constructor() {
        super()
        this.state = {
            data: {
                ...defaultData,
            }
            ,
            error: {
                ...defaultData,
            },
            filesToUploads: [],
            isValidForm: false
        };
    }

    validateForm = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case "name": {
                errMsg = this.state.data.name ? "" : "Name is Required";
                break;
            }
            case "price": {
                errMsg = this.state.data.price ? "" : "price is Required";
                break;
            }
            case "category": {
                errMsg = this.state.data.category ? "" : "category is Required";
                break;
            }
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg,
            }
        }), () => {
            let errors = Object.values(this.state.error).filter(err => err)
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleChange = (e) => {
        let { type, name, value, checked, files } = e.target;
        if (type === "checkbox") {
            value = checked;
        }

        if (type === "file") {
            const { filesToUploads } = this.state
            filesToUploads.push(files[0])
            this.setState({
                filesToUploads
            })
        }
        // console.log("name is ", name)
        // console.log("value is ", value)

        this.setState(preSate => ({
            data: {
                ...preSate.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    componentDidMount() {
        // console.log("productForm props", this.props);
        if (this.props.product) {
            this.setState({
                data: {
                    ...defaultData,
                    ...this.props.product,
                    disscountedItem: this.props.product.discount ? this.props.product.discount.discountedItem : "",
                    disscountType: this.props.product.discount ? this.props.product.discount.discountType : "",
                    disscountValue: this.props.product.discount ? this.props.product.discount.discountValue : "",
                    tags: (this.props.product.tags || []).toString(),
                    offers: (this.props.product.offers || []).toString(),
                }
            })
        }
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.submitFunction(this.state.data, this.state.filesToUploads);
    }
    render() {

        let discountdata = this.state.data.discountedItem ?
            <div className="form-group" style={{ width: "80%" }}>
                <label className="form-group">Discount Type</label>
                <select className="form-control" name="discountType" value={this.state.data.discountType} onChange={this.handleChange}>
                    <option>Choose Discount Type</option>
                    <option value="percentage">Percentage</option>
                    <option value="amount">Amount</option>
                    <option value="item">item</option>
                </select>
                <br></br>
                <label className="form-group">Discounted value</label>
                <input type="number" className="form-control" name="discountValue" value={this.state.data.discountValue} onChange={this.handleChange}></input>
            </div>
            : ""

        let warrentydata = this.state.data.warrentyStatus ?
            <div className="form-group" style={{ width: "80%" }}><label>Warrenty period</label>
                <input type="text" className="form-control" name="warrentyPeriod" value={this.state.data.warrentyPeriod} onChange={this.handleChange}></input>
            </div>
            : ""

        return (
            <div style={{ position: "absolute", left: "15%", top: "16%", width: "85%" }}>
                <h1>{this.props.title}</h1>
                <form className="container" onSubmit={this.handleSubmit} >
                    <label className="form-group">Name</label>
                    <input type="text" name="name" value={this.state.data.name} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">category</label>
                    <select name="category" value={this.state.data.category} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}>
                        <option>Choose Category</option>
                        <option value="electronics" >Electronics</option>
                        <option value="household" >household</option>
                        <option value="art and crafts" >art and crafts</option>
                        <option value="goods" >goods</option>
                        <option value="food" >food</option>
                        <option value="others">Others</option>
                    </select>
                    <br></br>
                    <label className="form-group">description</label>
                    <input type="text" name="description" value={this.state.data.description} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">price</label>
                    <input type="number" value={this.state.data.price} name="price" className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">brand</label>
                    <input type="text" name="brand" value={this.state.data.brand} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">Color</label>
                    <input type="text" name="color" value={this.state.data.color} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">model</label>
                    <input type="text" name="modelno" value={this.state.data.modelno} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">size</label>
                    <input type="text" name="size" value={this.state.data.size} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">tags</label>
                    <input type="text" name="tags" value={this.state.data.tags} className="form-control" style={{ width: "80%" }} onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-check-label" style={{ margin: "1% 2%" }}>
                        <input type="checkbox" className="form-check-input" checked={this.state.data.discountedItem} name="discountedItem" onChange={this.handleChange}></input>Discounted Item
                    </label>
                    <br></br>
                    {discountdata}
                    <br></br>
                    <label className="form-check-label" style={{ margin: "1% 2%" }}>
                        <input type="checkbox" className="form-check-input" name="warrentyStatus" checked={this.state.data.warrentyStatus} onChange={this.handleChange}></input>Warrenty Status
                    </label>
                    {warrentydata}
                    <br></br>
                    {
                        this.props.product && this.props.product.image && this.props.product.image.length && (
                            <>
                                <p>Previous Image</p>
                                <br></br>
                                <img src={`${ImageURL}${this.props.product.image[0]}`} alt="productImage.png" width="200px" height="200px"></img>
                            </>
                        )
                    }
                    <br></br>
                    <br></br>
                    <label>Add Image</label>
                    <input type="file" name="image" onChange={this.handleChange}></input>
                    <br></br>
                    <Button label="Add"></Button>
                </form>
            </div >
        )
    }
}
