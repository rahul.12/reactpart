import { now } from 'moment'
import React, { Component } from 'react'
import httpClient from '../../../utility/httpClient'
import notify from '../../../utility/notify'
import { Button } from '../../common/button/button.component'
import { ViewProduct } from "./../viewProduct/viewProduct"

const defaultData = {
    name: "",
    category: "",
    price: "",
    brand: "",
    minPrice: "",
    maxPrice: "",
    multipleDateRange: false,
    fromDate: "",
    toDate: "",
}
export class SearchProduct extends Component {
    constructor() {
        super()
        this.state = {
            data: {
                ...defaultData,
            },
            error: {
                ...defaultData,
            },
            allProduct: [],
            names: [],
            categories: [],
            searchResult: [],
            isSubmitting: false,
            isValidForm: false
        }
    }

    validateForm = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case "category": {
                errMsg = this.state.data.category ? "" : "category is reuired"
                break;
            }
            default: break
        }
        this.setState(preState => ({
            ...preState.error,
            [fieldName]: errMsg
        }), () => {
            let errs = Object.values(this.state.error).filter(err => err)
            this.setState({
                isValidForm: errs.length === 0
            })
        })
    }

    componentDidMount() {
        httpClient
            .POST("/product/search", {})
            .then(response => {
                // console.log("response is", response)
                let categories = []
                response.data.forEach((item) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category)
                    }
                })
                // console.log("catregories is", categories)
                this.setState({
                    allProduct: response.data,
                    categories: categories
                })
            })
            .catch(err => {
                notify.handleError(err)
            })
    }

    buildNameOptions = (categoryName) => {
        let names = this.state.allProduct.filter(item => item.category === categoryName)
        this.setState({
            names
        })
    }

    handleChange = (e) => {
        let { type, name, value, checked } = e.target;
        if (type === "checkbox") {
            value = checked
        }
        if (name === "category") {
            this.buildNameOptions(value)
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name)
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.setState({
            isSubmitting: true
        })
        if (!this.state.data.multipleDateRange) {
            // this.state.data.toDate = new Date(now())
            this.setState(preState => ({
                data: {
                    ...preState.data,
                    toDate: new Date(now())
                }
            }))
        }
        httpClient.POST("/product/search", this.state.data)
            .then(response => {

                this.setState({
                    searchResult: response.data
                })
                if (!response.data) {
                    notify.showInfo("No SUch Product Found")
                }
            })
            .catch(err => {
                notify.handleError(err)
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    render() {
        let content = this.state.searchResult.length
            ?
            <ViewProduct incomingData={this.state.searchResult}></ViewProduct>
            : <div style={{ position: "absolute", left: "15%", top: "16%", width: "85%" }}>
                <h1>Search Product</h1>
                <form className="container" onSubmit={this.handleSubmit} >
                    <label className="form-group">category</label>
                    <select name="category" className="form-control" onChange={this.handleChange}>
                        <option>Choose Category</option>
                        {
                            this.state.categories.map((item, i) => (
                                <option key={i} value={item}>{item}</option>
                            ))
                        }
                    </select>
                    <br></br>
                    {
                        this.state.data.category && (
                            <>
                                <label className="form-group">Name</label>
                                <select name="name" className="form-control" onChange={this.handleChange}>
                                    <option>Choose Category</option>
                                    {
                                        this.state.names.map((item, i) => (
                                            <option key={item._id} value={item.name}>{item.name}</option>
                                        ))
                                    }
                                </select>
                            </>
                        )
                    }

                    <br></br>
                    <label className="form-group">min price</label>
                    <input type="number" value={this.state.data.minPrice} name="minPrice" className="form-control" onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">max price</label>
                    <input type="text" name="maxPrice" value={this.state.data.maxPrice} className="form-control" onChange={this.handleChange}></input>
                    <br></br>
                    <label className="form-group">From Date</label>
                    <input type="date" name="fromDate" value={this.state.data.fromDate} className="form-control" onChange={this.handleChange}></input>
                    <br></br>
                    <div className="form-group">
                        <input type="checkbox" name="multipleDateRange" checked={this.state.data.multipleDateRange} className="form-check-input" onChange={this.handleChange}></input>
                        <label className="form-check-label">
                            MultipleDateRange
            </label>
                    </div>
                    <br></br>
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label className="form-group">To Date</label>
                                <input type="date" name="toDate" value={this.state.data.toDate} className="form-control" onChange={this.handleChange}></input>
                                <br></br>
                            </>
                        )
                    }
                    <Button isSubmitting={this.state.isSubmitting} isDisabled={!this.state.isValidForm} label="Search"></Button>
                </form>
            </div>
        return content

    }
}
