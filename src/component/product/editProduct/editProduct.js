import React, { Component } from 'react'
import httpClient from '../../../utility/httpClient';
import notify from '../../../utility/notify';
import { Loader } from '../../common/loader/loader.component';
import { ProductForm } from '../productForm/ProductForm'

export class EditProduct extends Component {
    constructor() {
        super();
        this.state = {
            product: "",
            isLoading: false
        }
    }

    componentDidMount() {
        // console.log("edit.props", this.props)
        const productId = this.props.match.params["id"]
        this.setState({
            isLoading: true
        })
        httpClient.GET(`/product/${productId}`, true)
            .then(response => {
                // console.log("response.data", response)
                this.setState({
                    product: response.data[0]
                })
            })
            .catch(err => {
                notify.handleError(err)
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (product, files) => {
        httpClient.UPLOADS("PUT", `/product/${product._id}`, product, files)
            .then(response => {
                // console.log("Response is>>", response)
                notify.showInfo("Product Updated")
                this.props.history.push("/view_product")
            })
            .catch(err => {
                notify.handleError(err)
            })
    }

    render() {
        let content = this.state.isLoading ? <Loader></Loader> : <ProductForm title="Edit Product" product={this.state.product} submitFunction={this.edit}></ProductForm>
        return (
            <>
                {content}
            </>
        )
    }
}
