import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import dateFormate from '../../../utility/dateFormate';
import httpClient from '../../../utility/httpClient';
import notify from '../../../utility/notify';
import { Loader } from '../../common/loader/loader.component';
const ImageURL = process.env.REACT_APP_IMAGE_URL

export class ViewProduct extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            products: []
        }
    }

    componentDidMount() {
        // console.log("this props", this.props)
        if (this.props.incomingData) {
            this.setState({
                products: this.props.incomingData
            })
        }
        else {
            this.setState({
                isLoading: true,
            })
            //hhtp call
            httpClient.GET("/product", true)
                .then(response => {
                    this.setState({
                        products: response.data
                    })
                    // console.log("res.data", response.data)
                })
                .catch(err => {
                    notify.handleError(err)
                })
                .finally(() => {
                    this.setState({
                        isLoading: false,
                    })
                })
        }

    }

    removeProduct = (id, index) => {
        httpClient.DELETE(`/product/${id}`, true)
            .then(response => {
                notify.showInfo(`product deleted`)
                const { products } = this.state
                products.splice(index, 1)
                this.setState({
                    products
                })
            })
            .catch(err => {
                notify.handleError(err)
            })
    }

    editProduct = (id) => {
        this.props.history.push(`/edit_product/${id}`)
    }
    render() {
        let content = this.state.isLoading ? <Loader /> : <>
            <table className="table">
                <thead>
                    <tr >
                        <th>SN</th>
                        <th>NAME</th>
                        <th>CATEGORY</th>
                        <th>CREATED Date</th>
                        <th>CREATED TIme</th>
                        <th>PRICE</th>
                        <th>IMAGE</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.products.map((item, i) => (
                        <tr key={item._id}>
                            <td>{i + 1}</td>
                            <td><Link to={`/product_details/${item._id}`}>{item.name}</Link></td>
                            <td>{item.category}</td>
                            <td>{dateFormate.formatDate(item.createdAt)}</td>
                            <td>{dateFormate.formatTime(item.createdAt)}</td>
                            <td>{item.price}</td>
                            <td><img src={`${ImageURL}${item.image[0]}`} alt="" width="200px" height="200px"></img></td>
                            <td>
                                <button className="btn btn-info" onClick={() => { this.editProduct(item._id) }}>Edit</button>
                                <button className="btn btn-danger" onClick={() => { this.removeProduct(item._id, i) }}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
        return (
            <div style={{ position: "absolute", left: "15%", top: "16%", width: "85%" }}>
                <h1>View Product here</h1>
                {content}
            </div>
        )
    }
}
