import React, { Component } from 'react'
import { ProductForm } from "../productForm/ProductForm"
import httpClient from "../../../utility/httpClient"
import notify from '../../../utility/notify'


export class AddProduct extends Component {
    constructor() {
        super();
        this.state = {}
    }

    componentDidMount() {
        // console.log("this.props", this.props)
    }

    add = (data, files) => {
        httpClient
            .UPLOADS("POST", `/product`, data, files)
            .then(Response => {
                // console.log("Respose product is >>", Response)
                notify.showSuccess("Product Added Successfully!")
                this.props.history.push("/view_product");
            })
            .catch(err => {
                notify.handleError(err)
            })
    }
    render() {
        return (
            <div >
                <ProductForm title="ADD PRODUCT" submitFunction={this.add} />
            </div>
        )
    }
}
