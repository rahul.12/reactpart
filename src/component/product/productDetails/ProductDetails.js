import React, { Component } from 'react'
import httpClient from '../../../utility/httpClient'
import notify from '../../../utility/notify'

export class ProductDetails extends Component {
    constructor() {
        super()
        this.state = {
            isLoading: false,
            product: ''
        }
    }

    componentDidMount() {
        // console.log("porps", this.props)
        this.setState({
            isLoading: true
        })
        httpClient.GET(`product/${this.props.match.params['id']}`, true)
            .then(res => {
                this.setState({
                    product: res.data,
                })
                // console.log("product", res.data)
            })
            .catch(err => {
                notify.handleError(err)
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    render() {
        // console.log("state is", this.state.product)
        let content = this.state.product ? (<div style={{ position: "absolute", left: "15%", top: "16%", width: "85%" }}>
            <h1>Product Details</h1>
            <table border="1px">
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>{this.state.product[0].name}</td>
                    </tr>
                    <tr>
                        <td>category</td>
                        <td>{this.state.product[0].category}</td>

                    </tr>
                    <tr>
                        <td>price</td>
                        <td>{this.state.product[0].price}</td>

                    </tr>
                    <tr>
                        <td>Brand</td>
                        <td>{this.state.product[0].brand}</td>

                    </tr>
                    <tr>
                        <td>color</td>
                        <td>{this.state.product[0].color}</td>

                    </tr>
                    <tr>
                        <td>Size</td>
                        <td>{this.state.product[0].size}</td>
                    </tr>
                </thead>
            </table>
        </div>) : ''
        return (
            <>
                {content}
            </>
        )
    }
}
