import React from "react"
import { Link } from "react-router-dom"
export const SideBar = () => {
    return (
        <>
            <div className="w3-sidebar w3-bar-block w3-large" style={{ width: 15 + '%', background: "#343a40", color: "white", height: "100%", float: "left", position: "relative", top: "0" }}>
                {/* <Link to="/add_product" className="w3-bar-item w3-button">Default</Link> */}
                <Link to="/dashboard" className="w3-bar-item w3-button w3-hover-none w3-hover-text-red text-decoration-none">HOME</Link>
                <Link to="/view_product" className="w3-bar-item w3-button w3-hover-none w3-hover-text-red text-decoration-none">VIEW PRODUCT</Link>
                <Link to="/search_product" className="w3-bar-item w3-button w3-hover-none w3-hover-text-red text-decoration-none">SEARCH PRODUCT</Link>
                <Link to="/add_product" className="w3-bar-item w3-button w3-hover-none w3-hover-text-red text-decoration-none">ADD PRODUCT</Link>
                <Link to="/notification" className="w3-bar-item w3-button w3-hover-none w3-hover-text-red text-decoration-none">NOTIFICATION</Link>
                <Link to="/message" className="w3-bar-item w3-button w3-hover-none w3-hover-text-red text-decoration-none">MESSAGE</Link>
            </div>
        </>
    )
}