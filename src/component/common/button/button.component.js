import React from "react";

export const Button = (props) => {
  let Label = props.label || "Login";
  let DisabledLabled = props.disabledLabled || "Submitting..";
  let btn = props.isSubmitting
    ? (<button disabled type="submit" className="btn btn-primary">{DisabledLabled}</button>)
    : (<button disabled={props.isDisabled} type="submit" className="btn btn-primary">{Label}</button>)

  return btn;
};
