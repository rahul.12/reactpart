import React, { Component } from 'react'
import "./MessageComponent.css"
import * as io from "socket.io-client"
import dateFormate from "./../../../utility/dateFormate"
import notify from '../../../utility/notify'
const socketURL = process.env.REACT_APP_SOCKET_URL
export class MessageComponent extends Component {
    constructor() {
        super()
        this.state = {
            data: {
                senderName: "",
                senderId: "",
                receiverName: "",
                receiverId: "",
                message: "",
                time: "",
            },
            messages: [],
            users: [],
            currentUser: {}
        }
    }

    componentDidMount() {
        const currentUser = JSON.parse(sessionStorage.getItem("user"))
        this.setState(preState => ({
            currentUser,
        }), () => {
            this.runSocket()
        })
    }
    runSocket() {
        this.socket = io(socketURL)
        this.socket.emit("new-user", this.state.currentUser.username)

        this.socket.on("reply-msg", (msgData) => {
            const { messages, data } = this.state
            messages.push(msgData)
            //swaping receiver
            data.receiverId = msgData.senderId
            this.setState({
                messages,
                data
            })
        })
        this.socket.on("reply-msg-own", (msgData) => {
            const { messages } = this.state
            messages.push(msgData)
            // no swaping receiver in own case
            this.setState({
                messages
            })
        })


        this.socket.on("users", (users) => {
            this.setState({
                users
            })
        })
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))
    }

    selectUser = (user) => {
        console.log("selected user is", user.name)
        this.setState(preState => ({
            data: {
                ...preState.data,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    send = (e) => {
        e.preventDefault()
        const { data, currentUser, users } = this.state
        if (!data.receiverId) {
            return notify.showInfo("Please,select a user First")
        }
        data.senderName = currentUser.username
        data.senderId = users.find(user => user.name === currentUser.username).id
        data.time = new Date()
        this.socket.emit("new-msg", data);
        this.setState(preState => ({
            data: {
                ...preState.data,
                message: ""
            }
        }))
    }
    render() {
        return (
            <div style={{ position: "absolute", left: "15%", top: "16%", width: "85%" }}>
                <h1>Lets Chat</h1>
                <div className="row">
                    <div className="col-md-6">
                        <ins>Message <span><strong>{this.state.currentUser.username}</strong></span></ins>
                        <div className="chat_box">
                            <ul>
                                {this.state.messages.map((message, i) => (
                                    <li key={i}>
                                        <p>{message.message}</p>
                                        <h6>{message.senderName}</h6>
                                        <h6>{dateFormate.formatTime(message.time)}</h6>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <form onSubmit={this.send} className="form-inline">
                            <input type="text" name="message" placeholder="Type Here.." className="form-control col-md-10" value={this.state.data.message} onChange={this.handleChange}></input>
                            <button type="submit" className="btn btn-success col-md-2">Send</button>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <ins>User</ins>
                        <div className="chat_box">
                            <ul>
                                {this.state.users.map((user, i) => (
                                    <li key={i}>
                                        <button className="btn btn-default" onClick={() => this.selectUser(user)}>{user.name}</button>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
