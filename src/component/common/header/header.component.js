import React from "react";
import "./header.component.css";

export const Heading = (props) => {
  return (
    <div className="nav_bar">
      <ul className="nav_list">
        <li className="nav_item">
          <a href="/home">Home</a>
        </li>
        <li className="nav_item">
          <a href="/contact">Contact</a>
        </li>
        <li className="nav_item">
          <a href="/about">About</a>
        </li>
        <li className="nav_item">
          <a href="/login">Login</a>
        </li>
      </ul>
    </div>
  );
};
