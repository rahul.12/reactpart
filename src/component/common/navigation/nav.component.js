import React from "react";
import { Link } from "react-router-dom";
import "./nav.component.css";
import { withRouter } from "react-router-dom";

const Logout = (props) => {
  sessionStorage.clear();
  props.history.push("/");
};

const NavbarComponent = (props) => {
  let btn = props.isLoggedIn ? (
    <div className="nav-item" id="nav-button">
      <button className="btn btn-dark" onClick={() => Logout(props)}>
        Logout
      </button>
    </div>
  ) : (
      <div className="nav-item" id="nav-button">
        <button className="btn btn-dark">
          <Link className="right-button" to="/">
            Login
        </Link>
        </button>
        <button className="btn btn-dark">
          <Link className="right-botton" to="/register">
            Sign Up
        </Link>
        </button>
      </div>
    );
  return (
    <div >
      <nav className="navbar navbar-expand-sm bg-dark" >
        <div className="nav-item" id="logo-div">
          <Link to="/home" className="navbar-brand">
            LOGO
          </Link>
        </div>
        <ul className="navbar-nav" id="nav-listMenu">
          <li className="nav-item">
            <Link className="nav-link" to="/home">
              HOME
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/music">
              MUSIC
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/download">
              DOWNLOAD
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/view_product">
              PRODUCT
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/about">
              ABOUT
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/contact">
              CONTACT
            </Link>
          </li>
        </ul>
        <div className="nav-item right-div" id="search-div">
          <input
            type="search"
            placeholder="Seacrh"
            name="search"
            id="searchBox"
            className="form-control"
          ></input>
          {/* btn type depending on logged in or not */}
          {btn}
        </div>
      </nav>
    </div>
  );
};

export const Navbar = withRouter(NavbarComponent);
