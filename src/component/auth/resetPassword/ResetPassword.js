import React, { Component } from "react";
import httpClient from "../../../utility/httpClient";
import notify from "../../../utility/notify";
import { Button } from "../../common/button/button.component";

const defaultData = {
    password: "",
    confirmPassword: ""
}
export class ResetPassword extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultData
            },
            error: {
                ...defaultData
            },
            isSubmitting: false,
            isValidForm: false,
        };
    }

    validateForm = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case "password": {
                errMsg = this.state.data.password ? "" : "Required Field*";
                break;
            }
            case "confirmPassword": {
                errMsg = this.state.data.confirmPassword === this.state.data.password ? "" : "Password Not Matched*";
                break;
            }
            default:
                break;
        }

        this.setState(
            (preState) => ({
                error: {
                    ...preState.error,
                    [fieldName]: errMsg,
                },
            }),
            () => {
                let errs = Object.values(this.state.error).filter((err) => err);
                this.setState({
                    isValidForm: errs.length === 0,
                });
            }
        );
    };

    componentDidMount() {
        this.id = this.props.match.params.id
    }
    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState(
            (preState) => ({
                data: {
                    ...preState.data,
                    [name]: value,
                },
            }),
            () => {
                this.validateForm(name);
            }
        );
    };

    handlesubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true,
        });
        httpClient
            .POST(`/auth/reset-password/${this.id}`, this.state.data)
            .then((res) => {
                notify.showInfo(
                    "Password reset successful,please Login!"
                );
                this.props.history.push("/");
            })
            .catch((err) => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false,
                });
            });
    };
    render() {
        return (
            <>
                <h1>Reset Password</h1>
                <p>Please,choose your password wisely</p>
                <form className="form-group" onSubmit={this.handlesubmit}>
                    <label>Password</label>
                    <input
                        type="password"
                        className="form-control col-md-8"
                        name="password"
                        onChange={this.handleChange}
                    ></input>
                    <p className="error">{this.state.error.password}</p>
                    <br></br>
                    <label>Confirm Password</label>
                    <input
                        type="password"
                        className="form-control col-md-8"
                        name="confirmPassword"
                        onChange={this.handleChange}
                    ></input>
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <br></br>
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                        label="Submit"
                    ></Button>
                </form>
            </>
        );
    }
}
