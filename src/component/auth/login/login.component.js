import React, { Component } from "react";
import "./login.component.css";
import { Button } from "../../common/button/button.component";
import { Link } from "react-router-dom";
import notify from "./../../../utility/notify.js";
import httpClient from "../../../utility/httpClient";

const defaultForm = {
  username: "",
  password: "",

};

export class LoginComponent extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        ...defaultForm,
      },
      error: {
        ...defaultForm,
      },
      isSubmitting: false,
      isValidForm: false,
      remember_me: false,
    };
  }

  handleChange = (e) => {
    // console.log("Target value", e.target);
    let { type, name, value, checked } = e.target;
    if (type === "checkbox") {
      value = checked;
    }
    this.setState(
      (preState) => ({
        data: {
          ...preState.data,
          [name]: value,
        },
        isSubmitting: false,
      }),
      () => {
        this.validateForm(name);
      }
    );
    // console.log("name is>>", name);
    // console.log("Value is >>", value);
  };

  componentDidMount() {
    if (sessionStorage.getItem("remember_me")) {
      this.props.history.push("/dashboard");
    }
  }

  validateForm = (fieldName) => {
    let errMsg;
    switch (fieldName) {
      case "username": {
        errMsg = this.state.data.username ? "" : "Username Required*";
        break;
      }
      case "password": {
        errMsg = this.state.data.password ? "" : "Please, Enter Password";
        break;
      }
      default:
        break;
    }
    this.setState(
      (preState) => ({
        error: {
          ...preState.error,
          [fieldName]: errMsg,
        },
      }),
      () => {
        const errors = Object.values(this.state.error).filter((err) => err);
        if (errors.length === 0) {
          this.setState({
            isValidForm: true,
          });
        }
      }
    );
  };

  handleSubmit = (e) => {
    e.preventDefault(); //prevent the default setting for html like page reload
    // console.log("this.state>>", this.state);
    httpClient
      .POST(`/auth/login`, this.state.data)
      .then((response) => {
        // console.log("login Response >>", response);
        notify.showSuccess(`Welcome ${response.data.user.username}`);
        sessionStorage.setItem("token", response.data.token);
        sessionStorage.setItem("user", JSON.stringify(response.data.user));
        sessionStorage.setItem("isLoggedIn", true);
        this.props.history.push("/dashboard");
      })
      .catch((err) => {
        notify.handleError(err);
      });
  };

  render() {
    return (
      <>
        <div className="container">
          <form onSubmit={this.handleSubmit} className="form-group">
            <div>
              <h1>Login</h1>
              <p>fill the information below</p>
              <label>Username or Email Address</label>
              <input
                className="form-control"
                type="text"
                name="username"
                onChange={this.handleChange}
              ></input>
              <p className="error">{this.state.error.username} </p>
              <br></br>
              <label>Password</label>
              <input
                type="password"
                name="password"
                onChange={this.handleChange}
                className="form-control"
              ></input>
              <p className="error">{this.state.error.password} </p>
              <br></br>
              <div className="form-check">
                <label className="form-check-label">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="remember_me"
                    onChange={this.handleChange}
                  ></input>
                  Remember Me
                </label>
              </div>
              <br></br>
              <Button
                isSubmitting={this.state.isSubmitting}
                isDisabled={!this.state.isValidForm}
                label="Sign In"
              ></Button>
            </div>
            <br></br>
            <div className="bottom-content">
              <Link className="bottom-link" to="/register">
                Don't have an account?
              </Link>
              <Link className="bottom-link" to="/forget-password">
                Forget Password
              </Link>
            </div>
          </form>
        </div>
        <br></br>
        <hr></hr>
        <footer>@Copyright information</footer>
      </>
    );
  }
}
