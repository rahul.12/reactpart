import React, { Component } from "react";
import httpClient from "../../../utility/httpClient";
import notify from "../../../utility/notify";
import { Button } from "../../common/button/button.component";

export class ForgetPassword extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        email: "",
      },
      error: {
        email: "",
      },
      isSubmitting: false,
      isValidForm: false,
    };
  }

  validateForm = (fieldName) => {
    let errMsg;
    switch (fieldName) {
      case "email": {
        errMsg = this.state.data.email ? "" : "Required Field*";
        break;
      }
      default:
        break;
    }

    this.setState(
      (preState) => ({
        error: {
          ...preState.error,
          [fieldName]: errMsg,
        },
      }),
      () => {
        let errs = Object.values(this.state.error).filter((err) => err);
        this.setState({
          isValidForm: errs.length === 0,
        });
      }
    );
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState(
      (preState) => ({
        data: {
          ...preState.data,
          [name]: value,
        },
      }),
      () => {
        this.validateForm(name);
      }
    );
  };

  handlesubmit = (e) => {
    e.preventDefault();
    this.setState({
      isSubmitting: true,
    });
    httpClient
      .POST("/auth/forget-password", this.state.data)
      .then((res) => {
        notify.showInfo(
          "Password reset email is sent to your email please check your inbox"
        );
        this.props.history.push("/");
      })
      .catch((err) => {
        notify.handleError(err);
      })
      .finally(() => {
        this.setState({
          isSubmitting: false,
        });
      });
  };
  render() {
    return (
      <>
        <h1>Forget Password</h1>
        <p>Please,enter your Email to recover your account</p>
        <form className="form-group" onSubmit={this.handlesubmit}>
          <label>Email Address</label>
          <input
            type="text"
            className="form-control col-md-8"
            name="email"
            onChange={this.handleChange}
          ></input>
          <p className="error">{this.state.error.email}</p>
          <br></br>
          <Button
            isSubmitting={this.state.isSubmitting}
            isDisabled={!this.state.isValidForm}
            label="Submit"
          ></Button>
        </form>
      </>
    );
  }
}
