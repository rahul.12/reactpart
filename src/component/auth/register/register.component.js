import React from "react";
import { Button } from "./../../common/button/button.component";
import "./register.component.css";
import { Link } from "react-router-dom";
import notify from "../../../utility/notify";
import httpClient from "../../../utility/httpClient";


const defaultData = {
  name: "",
  username: "",
  email: "",
  password: "",
  gender: "",
  dob: "",
  phoneNumber: "",
};

export class RegisterComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      data: {
        ...defaultData,
      },
      error: {
        ...defaultData,
        // username: "Required Field*",
        // password: "Required Field",
      },
      isSubmitting: false,
      isValidForm: false,
    };

    // console.log("Constructor at First");
  }

  //Component cycle
  componentDidMount() {
    // console.log("Initial Conditiona are appled here at Third");
    //props value always comes in componentDidMount(){} not in others.
    // console.log("this.props", this.props);
  }

  componentDidUpdate() {
    // console.log("Component Update");
  }

  componentWillUnmount() {
    //after Routing left
    // console.log("component Destroyed");
  }

  handleChange = (e) => {
    // console.log("Target>>", e.target);
    let { name, value } = e.target;
    this.setState(
      (preState) => ({
        data: {
          ...preState.data,
          [name]: value,
        },
      }),
      () => {
        this.validateForm(name); //validating form input
      }
    );
  };

  //form validation function
  validateForm = (name) => {
    let errMsg;
    switch (name) {
      case "username": {
        errMsg = this.state.data.username ? "" : "Username Required*";
        break;
      }
      case "password": {
        errMsg = this.state.data.password
          ? this.state.data.password.length < 6
            ? "Weak Password*"
            : ""
          : "password Required*";
        break;
      }

      default:
        break;
    }
    this.setState(
      (preState) => ({
        error: {
          ...preState.error,
          [name]: errMsg,
        },
      }),
      () => {
        const errors = Object
          .values(this.state.error)
          .filter((err) => err);
        this.setState({
          isValidForm: errors.length === 0,
        });
        // console.log("error>>", this.state);
      }
    );
  };

  //on submitting function
  handleSubmit = (e) => {
    e.preventDefault();
    console.log("this.state", this.state)
    //hhtp calling 
    httpClient
      .POST("/auth/register", this.state.data)
      .then(res => {
        console.log("response is >>", res);
        notify.showSuccess("Registration Successfull");
        this.props.history.push('/');
        console.log("i am below push")
      })
      .catch(err => {
        console.log("err is", err);
        notify.handleError(err);
      })

    // fetch(`${BaseURL}/auth/register`, {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     // 'Content-Type': 'application/x-www-form-urlencoded',
    //   },
    //   body: this.state.data //if any
    // })
    //   .then(function (response) {
    //     console.log("response is >>", response);
    //   })
    //   .catch(err => {
    //     notify.handleError(err)
    //   })
  };

  render() {
    // console.log("Render at Second");
    return (
      <>
        <div className="container">
          <form onSubmit={this.handleSubmit} className="form-container" noValidate>
            <h1 id="heading">Register</h1>
            <p>please register before login</p>
            <hr></hr>
            <label>Name</label>
            <input
              className="form-control"
              type="text"
              id="input-name"
              placeholder="Full Name"
              name="name"
              onChange={this.handleChange}
            ></input>
            <br></br>
            <label>Username</label>
            <input
              className="form-control"
              type="text"
              id="input-username"
              placeholder="Username"
              name="username"
              onChange={this.handleChange}
            ></input>
            {/* error msg testing */}
            <p className="error">{this.state.error.username}</p>
            <label>Email</label>
            <input
              className="form-control"
              type="email"
              id="input-email"
              placeholder="Email Address"
              name="email"
              onChange={this.handleChange}
            ></input>
            <br></br>
            <label>Password</label>
            <input
              className="form-control"
              type="password"
              id="input-password"
              placeholder="Password"
              name="password"
              onChange={this.handleChange}
            ></input>
            <p className="error">{this.state.error.password}</p>
            <label>Date Of Birth</label>
            <br></br>
            <input
              type="date"
              id="input-dob"
              name="dob"
              placeholder="date of Birth"
              onChange={this.handleChange}
            ></input>
            <br></br>
            <br></br>
            <label>Phone Number</label>
            <input
              className="form-control"
              type="number"
              id="input-phoneNumber"
              placeholder="Phone Number"
              name="phoneNumber"
              onChange={this.handleChange}
            ></input>
            <br></br>
            <label>Gender</label>
            <div className="form-check">
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  type="radio"
                  id="radio-male"
                  name="gender"
                  value="male"
                  onChange={this.handleChange}
                ></input>
                Male
              </label>
            </div>
            <div className="form-check">
              <label className="form-check-label">
                <input
                  className="form-check-input"
                  type="radio"
                  id="radio-female"
                  name="gender"
                  value="female"
                  onChange={this.handleChange}
                ></input>
                Female
              </label>
            </div>
            <br></br>
            <Button
              onSubmit={this.handleSubmit}
              isSubmitting={this.state.isSubmitting}
              isDisabled={!this.state.isValidForm}
              label="Register"
            ></Button>
          </form>
          <p className="bottom-content">
            <Link className="bottom-link" to="/">
              Already Registered?
            </Link>
          </p>
        </div>
        <br></br>
        <hr></hr>
        <footer>@Copyright information</footer>
      </>
    );
  }
}
