import React from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { ForgetPassword } from "./component/auth/forget_password/ForgetPassword";
import { LoginComponent } from "./component/auth/login/login.component";
import { RegisterComponent } from "./component/auth/register/register.component";
import { ResetPassword } from "./component/auth/resetPassword/ResetPassword";
import { MessageComponent } from "./component/common/message/MessageComponent";
import { Navbar } from "./component/common/navigation/nav.component";
import { SideBar } from "./component/common/sidebar/sidebarComponent";
import { AddProduct } from "./component/product/addProduct/addProduct";
import { EditProduct } from "./component/product/editProduct/editProduct";
import { ProductDetails } from "./component/product/productDetails/ProductDetails";
import { SearchProduct } from "./component/product/searchProduct/searchProduct";
import { ViewProduct } from "./component/product/viewProduct/viewProduct";

const HomePage = () => {
  return <h1>Home Page</h1>;
};
const MusicPage = () => {
  return <h1>Music Page</h1>;
};
const ProductPage = () => {
  return (
    <div>
      <SideBar isLoggedIn={sessionStorage.getItem("isLoggedIn")} />
    </div>
  );
};
const DownloadPage = () => {
  return <h1>Download Page</h1>;
};
const ContactPage = () => {
  return <h1>Contact Page</h1>;
};
const AboutPage = () => {
  return <h1>About Page</h1>;
};

const Dashboard = (props) => {
  // console.log("this dashProps", props);
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Dashboard Menu</h1>
      <p style={{ textAlign: "center" }}>i am your dashboard</p>
    </>
  );
};

//Not found pages
const NotFound = () => {
  return (
    <>
      <h1 style={{ textAlign: "center" }}>Page Not Found</h1>
      <img
        src="./image/NotFoundImage.jpg"
        alt="Not Found"
        width="80%"
        style={{ marginLeft: "10%" }}
      ></img>
    </>
  );
};

const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(routeProps) =>
        sessionStorage.getItem("isLoggedIn") ? (
          <>
            <div>
              <Navbar isLoggedIn={sessionStorage.getItem("isLoggedIn")} />
            </div>
            <div>
              <SideBar isLoggedIn={sessionStorage.getItem("isLoggedIn")} />
            </div>
            <div>
              <Component {...routeProps} />
            </div>
          </>
        ) : (
            <Redirect to="/" />
          )
      }
    ></Route>
  );
};

const PublicRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(routeProps) => (
        <>
          <div>
            <Component {...routeProps} />
          </div>
        </>
      )}
    ></Route>
  );
};

export const AppRouter = (props) => {
  return (
    <>
      <BrowserRouter>
        <Switch>
          {/* starting Route */}
          <PublicRoute exact path="/" component={LoginComponent}></PublicRoute>
          <PublicRoute
            path="/register"
            component={RegisterComponent}
          ></PublicRoute>
          <PublicRoute
            path="/forget-password"
            component={ForgetPassword}
          ></PublicRoute>
          <PublicRoute
            path="/reset-password/:id"
            component={ResetPassword}
          ></PublicRoute>

          {/* public Routes */}
          <PublicRoute path="/home" component={HomePage}></PublicRoute>
          <PublicRoute path="/music" component={MusicPage}></PublicRoute>
          <PublicRoute path="/download" component={DownloadPage}></PublicRoute>
          <ProtectedRoute
            path="/view_page"
            component={ProductPage}
          ></ProtectedRoute>
          <PublicRoute path="/contact" component={ContactPage}></PublicRoute>
          <PublicRoute path="/about" component={AboutPage}></PublicRoute>

          {/* proteted Routes */}
          <ProtectedRoute
            path="/dashboard"
            component={Dashboard}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/add_product"
            component={AddProduct}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/search_product"
            component={SearchProduct}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/view_product"
            component={ViewProduct}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/product_details/:id"
            component={ProductDetails}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/edit_product/:id"
            component={EditProduct}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/message"
            component={MessageComponent}
          ></ProtectedRoute>

          <PublicRoute component={NotFound}></PublicRoute>
        </Switch>
      </BrowserRouter>
    </>
  );
};
